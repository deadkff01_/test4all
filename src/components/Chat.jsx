import React from 'react'

const renderMessages = (messages) => {
    const list = messages || []
    return list.map(message => (
        <li class="left clearfix">
            <span class={`chat-img  ${message.displayPortraitLeft ? 'pull-left' : 'pull-right'}`}>
                <img src={message.portrait} alt="User Avatar" class="img-circle" />
            </span>
            <div class={`clearfix ${message.displayPortraitLeft ? 'chat-body' : ''}`}>
                <div class="header">
                    <strong class="primary-font">{message.userName}</strong>
                    <small class="text-muted">
                        <span class="glyphicon glyphicon-time"></span>{message.time}</small>
                </div>
                <p>
                    {message.message}
                </p>
            </div>
        </li>
    )
    )
}

const Chat = ({ messages, message, handleForm, handleChange }) => (
    <div class="col-md-8 chat-box mt-4 p-0 mb-5">
        <div class="panel panel-primary">
            <h5 className="chat-title"><i class="fa fa-comments mr-3"></i>Chat</h5>
            <div class="panel-collapse" id="collapseOne">
                <div class="panel-body">
                    <ul class="chat">
                        {renderMessages(messages)}
                    </ul>
                </div>
                <div class="panel-footer">
                    <form className="form" onSubmit={(e) => handleForm(e)}>
                        <div class="input-group">
                            <input
                                id="btn-input"
                                type="text"
                                name='message'
                                onChange={(e) => handleChange(e)}
                                value={message}
                                class="form-control input-sm"
                                placeholder="Type your message here..."
                                required />
                            <span class="input-group-btn">
                                <button type="submit" class="btn green-button btn-sm" id="btn-chat">
                                    Send</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
)

export default Chat