import React from 'react'
import numberFormat from '../utils/numberFormat'

const Widgets = ({ comments, newOrders, newUsers, pageViews }) => (
        <div className="mb-3">
            <h3 className="mt-4 mb-3">Dashboard</h3>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-3 mb-2">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="d-flex-centralizer icon-widget blue-layout col-xs-12 col-sm-5">
                                <i class="fa fa-shopping-bag"></i>
                            </div>
                            <div class="d-flex-centralizer text-widget col-xs-12 col-sm-7">
                            <div class="card-content">
                            <span class="d-block card-number">{numberFormat(newOrders)}</span>
                            <span class="d-block">New Orders</span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3 mb-2">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="d-flex-centralizer icon-widget yellow-layout col-xs-12 col-sm-5">
                                <i class="fa fa-comment"></i>
                            </div>
                            <div class="d-flex-centralizer text-widget col-xs-12 col-sm-7">
                            <div class="card-content">
                            <span class="d-block card-number">{numberFormat(comments)}</span>
                            <span class="d-block">Comments</span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3 mb-2">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="d-flex-centralizer icon-widget green-layout col-xs-12 col-sm-5">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="d-flex-centralizer text-widget col-xs-12 col-sm-7">
                            <div class="card-content">
                            <span class="d-block card-number">{numberFormat(newUsers)}</span>
                            <span class="d-block">New Users</span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3 mb-2">
                    <div class="card">
                        <div class="row no-gutters">
                            <div class="d-flex-centralizer icon-widget red-layout col-xs-12 col-sm-5">
                            <i class="fa fa-address-card"></i>
                            </div>
                            <div class="d-flex-centralizer text-widget col-xs-12 col-sm-7">
                            <div class="card-content">
                            <span class="d-block card-number">{numberFormat(pageViews)}</span>
                            <span class="d-block">Page Views</span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
         


            </div>
        </div>
)

export default Widgets