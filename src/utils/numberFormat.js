const numberFormat = (number) => {

    if(typeof number !== 'number') return 0

    let abs = Math.abs(number)
    const rounder = Math.pow(10, 1)
    const isNegative = number < 0
    let key = ''
    const powers = [
        {key: "Q", value: Math.pow(10,15)},
        {key: "T", value: Math.pow(10,12)},
        {key: "B", value: Math.pow(10,9)},
        {key: "M", value: Math.pow(10,6)},
        {key: "K", value: 1000}
    ]

    powers.map(p => {
        let reduced = abs / p.value
        reduced = Math.round(reduced * rounder) / rounder
        if(reduced >= 1){
            abs = reduced
            key = p.key
            return
        }
    })

    return (isNegative ? '-' : '') + abs + key
}

export default numberFormat
