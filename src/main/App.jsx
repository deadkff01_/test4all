import React, { Component } from 'react'
import axios from 'axios'
import '../common/dependencies'
import Widgets from '../components/Widgets'
import Chat from '../components/Chat'
import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'

import { API_URL, CHART_INITIAL_STATE } from '../consts'

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      widgets: {},
      chart: CHART_INITIAL_STATE,
      messages: [],
      message: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleForm = this.handleForm.bind(this)
  }

  async componentDidMount() {
    try {
      const widgets = await axios.get(API_URL + 'widgets')
      const pageViews = await axios.get(API_URL + 'pageViews')
      const messages = await axios.get(API_URL + 'messages')

      const categories = pageViews.data.map(p => (
        p.month
      ))
      const seriesValues = pageViews.data.map(p => (
        p.views
      ))

      this.setState({
        widgets: widgets.data,
        chart: {
          xAxis: {
            categories: categories
          },
          series: {
            data: seriesValues
          }
        },
        messages: messages.data
      })

    } catch (error) {
      throw error
    }
  }

  handleChange(e) {
    this.setState({ ...this.state, [e.target.name]: e.target.value })
  }


  async handleForm(e) {
    //avoid form refresh the page
    e.preventDefault()
    console.log(this.state)
    try {
      console.log(this.state.message)
      await axios.post(API_URL + 'messages', {
        message: this.state.message
      })
      
      const newMessage = {
        displayPortraitLeft: true,
        message: this.state.message,
        portrait: "http://placehold.it/50/FA6F57/fff&text=EU",
        time: "1 min ago",
        userName: "Eu"
      }
      // add new message and reset the input
      this.setState((prevState) => ({
        message: '',
        messages: prevState.messages.concat(newMessage)
      }))
    } catch (error) {
      throw error
    }

  }

  render() {
    const { comments, newOrders, newUsers, pageViews } = this.state.widgets
    return (
      <main className="container">
        {this.state.name}
        <Widgets
          comments={comments}
          newOrders={newOrders}
          newUsers={newUsers}
          pageViews={pageViews} />
        <HighchartsReact
          highcharts={Highcharts}
          options={this.state.chart}
        />
        <Chat
          messages={this.state.messages}
          message={this.state.message}
          handleForm={this.handleForm}
          handleChange={this.handleChange} />
      </main>
    )
  }
}
