export const API_URL = 'http://dev.4all.com:3050/'

export const CHART_INITIAL_STATE = {
    chart: {
        type: 'areaspline',
    },
    title: {
        text: 'Site Traffic Overview',
        align: 'left',
        y: 25,
        margin: 40
    },
    legend: {
        enabled: false
    },
    xAxis: {
        categories: []
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' units'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'Traffic',
        data: [],
    }]
}